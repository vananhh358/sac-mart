@extends('layout')

@section('content')

<br>

    <div class="container">
        <h1>Add Brand</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      
        <br>
        <form action="{{ route('brand.store') }}" method="POST" 
        style="background-color: rgba(20, 20, 20, 0.4);
        width: 40%;
        margin: 10px auto;
        padding: 20px; 
        border: 1px solid white; 
        border-radius: 20px;">
            @csrf
            <h3><span>Name</span></h3>
            <input type="text" class="form-control" name="name" placeholder="Enter brand's name">
            <br>
            <button type="submit" class="btn">Submit</button>
        </form>
        <div class="container-fluid px-0">
        <a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>
        </div>
    </div>
@endsection
