@extends('layout')

@section('content')
<div class="container">
        <h1>Edit</h1>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="container-fluid px-0">
        <a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>
        </div>
        <form action="{{ route('brand.update', $brand->id) }}" method="POST" 
        style="background-color: rgba(20, 20, 20, 0.4);
        width: 40%;
        margin: 10px auto;
        padding: 20px; 
        border: 1px solid white; 
        border-radius: 20px;">
            @csrf
            @method('PUT')
            <span>Name</span>
            <input type="text" class="form-control" value="{{ $brand->name }}" name="name" placeholder="Enter brand's name">
            <button type="submit" class="btn">Submit</button>
        </form>
    </div>
@endsection
