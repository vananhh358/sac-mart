@extends('layout')

@section('content')
<br>
    <div class="container">
        <div class="container"><h1>Brands</h1></div>
        <a class="btn" href="{{ route('brand.create') }}">Add New Brand</a>
        <br>
        <div class="container">
                    @foreach($brands as $brand)
            <div class="detail">
                    <h1>Brand: {{ $brand->name }}</h1>
                    
                @foreach($brand->laptops as $laptop)
                 <h3><a href="{{ route('laptop.show',$laptop->id) }}" style="color: white">{{ $laptop->name }}</a></h3>
                @endforeach
                
                    <td>
                        <form action="{{ route('brand.destroy', $brand->id) }}" method="POST">    
                            <a class="btn" href="{{ route('brand.edit', $brand->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button class="btn" onclick="return confirm('Are you sure you want to Delete this?')">Delete</button>
                        </form>
                    </td>
                    </div>
            @endforeach
        </table>
        <a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>
        
        </div>

        {{ $brands->links('pagination::bootstrap-4') }}
    </div>
@endsection
