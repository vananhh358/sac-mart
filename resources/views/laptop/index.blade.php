@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left text-center">
                <h3 style="font-size: 300%">Laptops</h3>
            </div>    
        </div>
    </div>
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <span>{{ $message }}</span>
    </div>
    @endif
    <table class="table">
        <tr>
            <th>Image</th>
            <th>Name</th>
            <th>Brand</th>
            <th>Price</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        @foreach ($laptops as $laptop)
        <tr>
            <td><img width="100" height="100" src="{{ asset('images/' . $laptop->image) }}"></td>
            <td>{{ $laptop->name }}</td>
            <td>{{ $laptop->brand->name }}</td>
            <td>{{ $laptop->price }}</td>
            <td>{{ $laptop->description }}</td>
            <td><form action="{{ route('laptop.destroy',$laptop->id) }}" method="POST" style="border: none">           
            <a class="btn" href="{{ route('laptop.show',$laptop->id) }}">&#128466;</a>
            <a class="btn" href="{{ route('laptop.edit',$laptop->id) }}">&#128396;</a>
            @csrf
            @method('DELETE')
            <button type="submit" onclick="return confirm('Are you sure you want to Delete this?')" class="btn">&#128465;</button>
            </form>
            </td>
        </tr>
        @endforeach
    </table>
    <div class="row justify-content-center">
{{ $laptops->links('pagination::bootstrap-4') }}
</div>
@endsection