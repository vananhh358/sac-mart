@extends('layout')
@section('content')
<br>
    <div class="row text-center">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h1 style="font-size: 300%">{{ $laptop->name }} Detail</h1>
            </div>
        </div>
    </div>
    <br>
    <div class="row">
    <div class="col-sm-4">       
        <img width="400" height="400" src="{{ asset('images/' . $laptop->image) }}">
        </div>
    <div class="col-sm-8">
    <div class="container">
    <div class="detail">
    
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h2>Name: {{ $laptop->name }}</h2>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h4>Brand: {{  $laptop->brand->name }}</h4>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h4>Price: {{ $laptop->price }}</h4>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <h4>Description: {{ $laptop->description }}</h4>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
         
        <td><form action="{{ route('laptop.destroy',$laptop->id) }}" method="POST" style="border: none">                     
            <a class="btn" href="{{ route('laptop.edit',$laptop->id) }}">&#128396;</a>
            @csrf
            @method('DELETE')
            <button type="submit" onclick="return confirm('Are you sure you want to Delete this?')" class="btn">&#128465;</button>
            </td>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
        <a href="#" onclick="history.back();"><button type="button" name="back" class="btn">Back</button></a>
    </div>
    </div>
    </div>
   
    </div>
    </div>
    
@endsection