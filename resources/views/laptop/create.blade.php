@extends('layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h1 style="font-size: 300%">Add New Product</h1>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
        <strong>Whoops!</strong> There are problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
        </div>
    @endif
    <form action="{{ route('laptop.store') }}" method="POST" enctype="multipart/form-data" 
    style="background-color: rgba(20, 20, 20, 0.4);
    width: 40%;
    margin: 10px auto;
    padding: 20px; 
    border: 1px solid white; 
    border-radius: 20px;">
    @csrf
    <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name</strong>
                    <input type="text" name="name" class="form-control" placeholder="Enter laptop's name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Brand</strong>
                    <select name="brand_id" class="form-control">
                @foreach($brand as $brand)
                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                @endforeach
                </select>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Price</strong>
                    <input type="text" name="price" class="form-control" placeholder="Enter laptop's price">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Description</strong>
                <input type="text" name="description" class="form-control" placeholder="More about this laptop">
            </div>         
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                <strong>Image</strong>
                <input type="file" name="image" class="form-control">
            </div>         
            </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn">Submit</button>
            <a class="btn" href="{{ route('laptop.index') }}"> Back</a>
        </div>
    </div>
    </form>
@endsection