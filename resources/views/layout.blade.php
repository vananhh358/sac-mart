<!DOCTYPE html>
<html>
    <head>

<!-- tạo nav bar -->
<nav class="navbar navbar-expand-lg navbar-black bg-black">
<h1> <img src="/images/logo_3.png" ></h1>

  <a class="navbar-brand" style="font-size: 40px; color: white" href="{{ route('laptop.index') }}">SAC Mart</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link disabled" style="font-size: 20px">Sáng Bright Like A Diamond</a>
      </li>
    </ul>
    <form action="{{ route('laptop.index') }}" method="GET" role="search" class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" name="term" placeholder="Search" aria-label="Search" id="term">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
        <title>SAC Mart</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">  
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet"/>
<!-- </head> -->


        <!-- Tạo side bar -->

<!-- <body> -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
  font-family: "Lato", sans-serif;
}

.sidebar {
  height: 100%;
  width: 200px;
  position: fixed;
  z-index: 1;
  top: 50;
  left: 0;
  background-color: #111;
  overflow-x: hidden;
  transition: 0.5s;
  
}

.sidebar a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 23px;
  color: #818181;
  display: block;
  transition: 0.3s;
  border: none;
  background: none;
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
}

.sidebar a:hover, .dropdown-btn:hover {
  color: #f1f1f1;
}

.left {
  width: 70%;
  background: green;
  padding-right:5px;
  box-sizing: border-box;
}

.right {
  width: 30%;
  position: absolute;
  top: 0px;
  right: 0;
  background: yellow;
  height:100%;
}

.sidebar .closebtn {
  position: right;
  top: 0;
  right: 0px;
  font-size: 36px;
  margin-left: 170px;
}

.openbtn {
  font-size: 20px;
  cursor: pointer;
  background-color: #111;
  color: white;
  padding: 10px 15px;
  border: none;
  
}

.openbtn:hover {
  background-color: #444;
}

/* Add an active class to the active dropdown button */
.active {
  
  color: white;
}
/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #262626;
  padding-left: 8px;
}
/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}
/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
  .sidebar {padding-top: 15px;}
  .sidebar a {font-size: 18px;}
}
</style>
</head>




<div>
  <div id="main" style="position: absolute; left:1.7px">
    <button class="openbtn" onmouseover="openNav()">☰</button>  
  </div>
  <div id="mySidebar" class="sidebar" style="position: absolute">
  
  <a href="javascript:void(0)" class="closebtn" onmouseover="closeNav()">×</a>
  <a href="{{ route('laptop.index') }}">Home</a>
  <button class="dropdown-btn">Brands
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="{{ route('brand.index') }}">All Brands</a>
    <a href="{{ route('brand.create') }}">Add New Brands</a>
  </div>
  <button class="dropdown-btn">Laptops
    <i class="fa fa-caret-down"></i>
  </button>
  <div class="dropdown-container">
    <a href="{{ route('laptop.index') }}">All Laptops</a>
    <a href="{{ route('laptop.create') }}">Add Laptop</a>
  </div>
  </div>
</div>

<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>
   



    <!-- liên -->
<body>
<style>
head {
    color: white;
    text-align: center;
}
body {
    font-family: Georgia;
    font-size: 18px;
    color:  white;
    text-align: center;
    background-size: 100% 100%;
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-image: url('https://wallup.net/wp-content/uploads/2017/11/23/526001-computer-keyboards.jpg');
}
table{
    background-color: rgba(20, 20, 20, 0.3);
    margin: 50px 10px 50px 10px;
    text-align: left;
    color: white;
    border: 1px solid white;    
}
tr {
    background-color: rgba(20, 20, 20, 0.2);
    margin: 20px auto;
    text-align: left;
    color: white;
    border: 1px solid white;   
}
th, td{
    border: none;
    height: 30px;
    padding: 2px;
}

.input-group {
    margin: 10px 0px 5px 0px;
}
.input-group label {
    display: block;
    text-align: left;
    margin: 2px;
    color:  white;
}
.input-group input {
    height: 30px;
    width: 100%;
    padding: 5px 10px;
    font-size: 16px;
    border-radius: 5px;
    border: none;
}
.btn {
    margin: 5px auto;
    padding: 10px 15px;
    font-size: 20px;
    color: white;
    background-color: rgba(20, 20, 20, 0.4);
    border: 1px solid white;
    border-radius: 5px;
}

.detail {
    margin: 10px 30px;
    padding: 20px; 
    border: 1px solid white; 
    border-radius: 20px; 
    background-color: rgba(20, 20, 20, 0.4);
}
</style>
<div class="container">
    @yield('content')
</div>
</body>
</html>