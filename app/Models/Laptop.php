<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Laptop extends Model
{
  protected $table = 'laptop';
  protected $fillable = ['name','brand_id','price','description','image'];
  
  public function brand()
  {
      return $this->belongsTo(Brand::class, 'brand_id', 'id');
  }

}
