<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLaptopRequest;
use App\Http\Requests\UpdateLaptopRequest;
use App\Models\Brand;
use App\Models\Laptop;
use Illuminate\Http\Request;


class LaptopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $laptops = Laptop::with('brand');
        $laptops = Laptop::where([
            ['name', '!=', Null],
            [function ($query) use ($request) {
                if (($term = $request->term)) {
                    $query->orWhere('name', 'LIKE', '%' . $term . '%')->get();
                }
            }]
        ])
            ->paginate(5);

        return view('laptop.index', compact('laptops'));

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brand = Brand::get();
        return view('laptop.create', compact('brand'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateLaptopRequest $request)
    {
       

        $imageName = time() . '-' . $request->name . '.' . $request->image->extension();

        $request->image->move(public_path('images'),$imageName);
        
        $laptop = Laptop::create([
            'name' => $request->input('name'),
            'brand_id' => $request->input('brand_id'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'image' => $imageName
        ]);
                
        return redirect()->route('laptop.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Laptop  $laptop
     * @return \Illuminate\Http\Response
     */
    public function show(Laptop $laptop)
    {
        return view('laptop.show',compact('laptop'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Laptop  $laptop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $laptop = Laptop::findOrFail($id);
        $brand = Brand::get();
        return view('laptop.edit', compact('laptop', 'brand'));
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Laptop  $laptop
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLaptopRequest $request, $id)
    {
       
        $laptop=Laptop::findOrFail($id);
        $laptop->name = $request->input('name');
        $laptop->brand_id = $request->input('brand_id');
        $laptop->price = $request->input('price');
        $laptop->description = $request->input('description');
        
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '-' . $request->name . '.' . $extension;
            $file->move(public_path('images'), $filename);
            $laptop->image = $filename;
        }
        $laptop->save();
        return redirect()->route('laptop.index');
    
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Laptop  $laptop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Laptop::destroy($id);
        return redirect()->route('laptop.index');

    }
    
}