<?php
use App\Http\Controllers\BrandController;
use App\Http\Controllers\LaptopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('laptop', 'App\Http\Controllers\LaptopController');
Route::resource('brand', 'App\Http\Controllers\BrandController');

